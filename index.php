<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="Description" content="EDC contactformulier">

    <title>EDC contactformulier assesment</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="/js/main.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1>Contact formulier</h1>
            <form id="contact-form" method="post" novalidate>

                <div class="form-group">
                    <label for="name">Naam*</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Naam">
                    <div class="error invalid-feedback"></div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-8">
                        <label for="street">Straat*</label>
                        <input type="text" class="form-control" id="street" name="street" placeholder="Straat">
                        <div class="error invalid-feedback"></div>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="housenummer1">Huisnummer*</label>
                        <input type="number" class="form-control" id="housenummer1" name="housenummer1"placeholder="Huisnummer">
                        <div class="error invalid-feedback"></div>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="housenummer2">Toevoeging</label>
                        <input type="text" class="form-control" id="housenummer2" name="housenummer2" placeholder="Toevoeging">
                        <div class="error invalid-feedback"></div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="zipcode">Postcode*</label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" placeholder="Postcode">
                        <div class="error invalid-feedback"></div>
                    </div>
                    <div class="form-group col-md-8">
                        <label for="city">Woonplaats*</label>
                        <input type="text" class="form-control" id="city" name="city" placeholder="Woonplaats">
                        <div class="error invalid-feedback"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="country">Land</label>
                    <select id="country" name="country" class="form-control">
                        <option value="">Selecteer een land</option>
                        <option value="NL">Nederland</option>
                        <option value="BE">Belgie</option>
                    </select>
                    <div class="error invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label for="email">Email*</label>
                    <input type="Email" class="form-control" id="email" name="email" placeholder="Email">
                    <div class="error invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label for="message">Bericht*</label>
                    <textarea class="form-control" id="message" name="message" rows="3"></textarea>
                    <div class="error invalid-feedback"></div>
                </div>
                <button type="submit" id="target" class="btn btn-primary">Neem contact op</button>
            </form>
            <div class="d-none confirmation-message">
                <p>Uw bericht is verzonden er wordt binnenkort contact met u opgenomen.</p>
            </div>
        </div>
    </div>
</div>
</body>
</html>