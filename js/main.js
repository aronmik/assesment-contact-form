/**
 * Error messages to be displayed after encountering an error
 */
var requiredText = "Dit veld is verplicht";
var numericText = "Vul hier een nummer in";
var emailText = "Het door u ingevulde emailadres is niet in het correcte formaat";
var zipcodeText = "Vul een correcte postcode in";
var countryText = "U heeft geen land geselecteerd";
var errors = false;

/**
 * Var to be filled with the rules from the retrieved config
 */
var validationRules = {};
var allowedCountries = [];

/**
 * After document is loaded this function will load the config file.
 *
 * And set the handlers for the contact form
 */

$( document ).ready(function() {
    $.getJSON( "/ajax/config.php", function( data ) {
        validationRules = data.rules;
        allowedCountries = data.countries
    });

    $("#contact-form").submit(function (event) {
        event.preventDefault();
        validate();
        post();
    });

    $(":input").focus(function (event) {
        $(this).removeClass('is-invalid');
    });
});

/**
 * Handles the different validation rules
 */
function validate() {
    resetValidation();

    $.each( validationRules, function(input, value ){
        let target = $('#' + input);
        if(value.includes("numeric")) {
            isNotNumeric(target);
        }
        if(value.includes("zipcode")) {
            isNotZipcode(target);
        }
        if(value.includes("email")) {
            isNotEmail(target);
        }
        if(value.includes("required")) {
            isEmpty(target);
        }
        if(value.includes("allowed-countries")) {
            countryNotAllowed(target);
        }
    });

    setValid();
}

/**
 * Submits the form and checks for errors from the backend. If zero errors show confirmation message.
 *
 * If any errors are encountered do the client side validation again.
 */
function post() {
    if(!errors) {
        $.post( "/ajax/post.php", $( "#contact-form" ).serialize(), function( data ) {
            if(1 == data) {
                $('.confirmation-message').removeClass('d-none');
                $('#contact-form').addClass('d-none');
            }
            else {
                validate();
            }
        });
    }
}

/**
 * Adds the is-valid class to all the inputs without a validation error
 */
function setValid() {
    $(":input").not('.is-invalid').addClass('is-valid');
}

/**
 * Resets the form so wehnvalidation happens again all visuals are updated correctly
 */
function resetValidation() {
    errors = false;
    $(".is-invalid").removeClass('is-invalid');
    $(".is-valid").removeClass('is-valid');
    $(".error").html('');
}

/**
 * Checks if the value from the input is not a number
 */
function isNotNumeric(input) {
    if(isNaN(input.val())) {
        handleError(input,numericText);
        return true;
    }
    return false
}

/**
 * Checks if the value from the input is not an email
 */
function isNotEmail(input) {
    let regex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    if(!regex.test(input.val())) {
        handleError(input,emailText);
        return true;
    }
    return false;
}


/**
 * Checks if the value from the input is a correct zipcode
 * Uses the country input to determine for wich country to check. Default checks for NL zipcode.
 */

function isNotZipcode(input) {
    let regex = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i;

    if('BE' == $('#country').val()) {
        regex = /^(?:(?:[1-9])(?:\d{3}))$/;
    }

    if(!regex.test(input.val())) {
        handleError(input,zipcodeText);
        return true;
    }
    return false;
}

/**
 * Checks if the value from the input is not empty
 */
function isEmpty(input) {
    if(0  === input.val().length) {
        handleError(input,requiredText);
        return true;
    }
    return false;
}


/**
 * Checks if the value from the input is an allowed country
 */
function countryNotAllowed(input) {
    if(!allowedCountries.includes(input.val())) {
        handleError(input,countryText);
        return true;
    }
    return false;
}

/**
 * Handles the needed steps to give the user feedback for the error that was encountered
 */
function handleError(input,text) {
    errors = true;
    input.addClass('is-invalid');
    input.siblings(".error").html(text);
}

