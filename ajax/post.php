<?php
require_once ($_SERVER['DOCUMENT_ROOT'] . '/config/init.php');

if(isset($_POST['name'])) {
    $validation = New Validation();

    if($validation->validate($_POST)) {
        $mail = new Mail();
        $mail->send($_POST);
        echo true;
    }
    else {
        echo json_encode($validation->getErrors());
    };
}
