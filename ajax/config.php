<?php
require_once ($_SERVER['DOCUMENT_ROOT'] . '/config/init.php');

$validation = New Validation();
$json = [
    'rules' => $validation->getRules(),
    'countries' => $validation->getAllowedCountries()
];

echo json_encode($json);