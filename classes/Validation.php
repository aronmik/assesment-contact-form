<?php

/**
 * Class Validation
 * Responsible for validating the input and setting the rules for validation.
 */
class Validation {

    /**
     * @var array Rules for validating the different inputs
     */
    private $validationRules = [
        'name' => ['required'],
        'street' => ['required'],
        'housenummer1' => ['required','numeric'],
        'zipcode' => ['required','zipcode'],
        'city' => ['required'],
        'country' => ['required','allowed-countries'],
        'email' => ['required', 'email'],
        'message' => ['required']
    ];
    /**
     * @var array List of allowed counties
     */
    private $allowedCountries = ['NL','BE'];
    /**
     * @var bool Tells if the validation was successful
     */
    private $validated = true;
    /**
     * @var array Gets filled with alle the errors the validation process encounters
     */
    private $errors = [];

    /**
     * Validates the post variables
     *
     * @param $post Raw post vars from the contact form
     * @return bool Results of validation
     */
    public function validate($post) {

        $this->checkRules($post);

        return $this->validated;
    }

    /**
     * Checks all the rules
     *
     * @param $post Post vars from the contact form
     */
    private function checkRules($post) {
        foreach($this->validationRules as $name => $rules) {
            $value = htmlspecialchars($post[$name]);
            foreach($rules as $rule) {
                if('required' === $rule) {
                    $this->isEmpty($value,$name);
                }
                if('numeric' === $rule) {
                    $this->isNotNumeric($value,$name);
                }
                if('zipcode' === $rule) {
                    $this->isNotZipcode($value,$post['country'],$name);
                }
                if('email' === $rule) {
                    $this->isNotEmail($value,$name);
                }
                if('allowed-countries' === $rule) {
                    $this->countryNotAllowed($value,$name);
                }
            }
        }
    }

    /**
     * Checks if a variable is not numeric
     *
     * @param $value The value to be check for being a number
     * @param $name The name of the input this value comes from
     * @return bool Validation result
     */
    private function isNotNumeric($value, $name) {
        if(!is_numeric($value)) {
            $this->validated = false;
            $this->errors[$name][] = 'numeric';
            return true;
        }

        return false;
    }

    /**
     * Checks if a variable is empty
     *
     * @param $value The value to be check if empty
     * @param $name The name of the input this value comes from
     * @return bool Validation result
     */
    private function isEmpty($value, $name) {
        if('' === $value) {
            $this->validated = false;
            $this->errors[$name][] = 'required';
            return true;
        }

        return false;
    }

    /**
     * Checks if the zipcode is correct
     * Needs the country param to determine for wich zipcode to check
     *
     *
     * @param $value The value that has to be checked if correct zipcode
     * @param $country The country that was recieved as value
     * @param $name The name of the input this value comes from
     * @return bool Validation result
     */
    private function isNotZipcode($value, $country, $name) {
        $regex = "/^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i";

        if('BE' == $country) {
            $regex = "/^(?:(?:[1-9])(?:\d{3}))$/";
        }

        if(!preg_match($regex,$value)) {
            $this->validated = false;
            $this->errors[$name][] = 'zipcode';
            return true;
        }

        return false;
    }

    /**
     * Checks if the variable is a correctly formated email
     *
     * @param $value The value to be checked if a correct email
     * @param $name The name of the input this value comes from
     * @return bool Validation result
     */
    private function isNotEmail($value, $name) {
        $regex = "/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/";

        if(!preg_match($regex,$value)) {
            $this->validated = false;
            $this->errors[$name][] = 'email';
            return true;
        }

        return false;
    }

    /**
     * Checks if the country is within the allowed country list
     *
     * @param $value The value to be checked if its an allowed country
     * @param $name The name of the input this value comes from
     * @return bool Validation result
     */
    private function countryNotAllowed($value, $name) {
        if(!in_array($value,$this->allowedCountries)) {
            $this->validated = false;
            $this->errors[$name][] = 'allowed-countries';
            return true;
        }

        return false;
    }

    /**
     * Returns a list of all the rules as defined in the class
     *
     * @return array A list of rules for validation
     */
    public function getRules() {
        return $this->validationRules;
    }

    /**
     * Returns a list of all the countries that are allowed as defined in the class
     * @return array A list of the allowed countries
     */
    public function getAllowedCountries() {
        return $this->allowedCountries;
    }

    /**
     * Returns a list of all the errors encountered. If called before validating will return an empty list.
     *
     * @return array A list of all the errors encountered during validation
     */
    public function getErrors() {
        return $this->errors;
    }

}