<?php

/**
 * Class Mail
 */
class Mail {
    /**
     * @param $post The post request from the contact form
     * Function is responsible for sending the email
     */
    public function send($post) {
        $message = "
        Contactformulier
        
        " . htmlspecialchars($_POST['name']) . " \r
        " . htmlspecialchars($_POST['street']) . " " . htmlspecialchars($_POST['housenummer1']) . "" . htmlspecialchars($_POST['housenummer2']) . " \r
        " . htmlspecialchars($_POST['zipcode']) . " " . htmlspecialchars($_POST['country']) . "  \r
        " . htmlspecialchars($_POST['email']) . " \n
        Bericht: \r
        " . htmlspecialchars($_POST['message']) . " \n";

        //Geen mail server op xampp beschikbaar
        //mail('a.mik@lastweb.nl', 'Contact formulier', $message);
    }
}